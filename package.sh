#pyinstaller -F -w -n mrc -i resources\logo.ico main_window.py

# mkdir 
rm -rf mrc-v0.1.0 mrc-v0.1.0.tar.gz
mkdir mrc-v0.1.0

# collect
cp dist/mrc.exe mrc-v0.1.0
cp -r config.yml mrc-v0.1.0/
cp -r README.md mrc-v0.1.0/README.txt
cp -r resources mrc-v0.1.0/

# create
tar -cjvf mrc-v0.1.0.tar.gz mrc-v0.1.0
