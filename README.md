# magazine_ref_count

#### 介绍
杂志引用统计

#### 安装使用

1. release 页面下载压缩包
2. 解压点击 .exe 文件运行
3. 根据 config.yml 的配置来检索统计
4. 会选取源文件夹下所有 .fbd 文件来检索
5. 若设置 config.yml 中 similarity_threshold 的值大于 1， 只有检索功能没有分类功能
6. similarity_threshold 越小，分类的越强，错误率越高。目前取用 0.6
