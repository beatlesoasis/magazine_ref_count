import unittest
import references_slicer

class TestRefSlicer(unittest.TestCase):
    def setUp(self):
        # start_expr = r'HTH〗参〓考〓文〓献'
        # end_expr = r'〖HT10.SS〗建筑结构学报〓'
        # target = r'［[0-9]{,5}］'
        self._slicer = references_slicer.RefSlicer(r'HTH〗参〓考〓文〓献',
                                                r'〖HT10.SS〗建筑结构学报〓',
                                                r'［[0-9]{1,5}］',
                                                r'')
        self._slicer.open("resources/test_input_gb18030.fbd")

    def test_open(self):
        return

    def test_find_all_refs(self):
        self._slicer.find_all_refs()
        for v in self._slicer._ref_list:
            print("%s\n"%(v))

if __name__ == "__main__":
    unittest.main()